package com.study.web.shop.web.filter;

import com.study.web.shop.service.security.DefaultSecurityService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class SecurityFilter implements Filter {
    private final static String USER_TOKEN_NAME = "user-token";
    private DefaultSecurityService securityService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("filter");

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if("POST".equals(httpServletRequest.getMethod()) && "/login".equals(httpServletRequest.getServletPath())){
            httpServletRequest.getRequestDispatcher("/login").forward(httpServletRequest, httpServletResponse);
        }

        Cookie[] cookies = httpServletRequest.getCookies();
        String token = null;
        if (cookies != null) {
            token = Arrays.stream(cookies)
                    .filter(c -> USER_TOKEN_NAME.equals(c.getName()))
                    .findAny()
                    .map(Cookie::getName)
                    .orElse(null);
        }

        if (token == null || !securityService.isAuthenticated(token)) {
            // return to login page
            httpServletRequest.getRequestDispatcher("/login").forward(httpServletRequest, httpServletResponse);
//            httpServletResponse.sendRedirect("/login");
        } else if (securityService.isAuthorized(token, httpServletRequest.getServletPath())) {
            // forward if authorized
            httpServletRequest.getRequestDispatcher(httpServletRequest.getServletPath()).forward(httpServletRequest, httpServletResponse);
//            chain.doFilter(request, response);
        } else {
            //todo: redirect to "Unauthorized"
            httpServletResponse.sendRedirect("/login");
        }
    }

    @Override
    public void destroy() {

    }

    public void setSecurityService(DefaultSecurityService securityService) {
        this.securityService = securityService;
    }
}
