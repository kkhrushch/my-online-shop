package com.study.web.shop.web.servlet.security;

import com.study.web.shop.service.security.DefaultSecurityService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class SecurityServlet extends HttpServlet {
    private final static String USER_TOKEN_NAME = "user-token";
    private DefaultSecurityService securityService;

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String token = null;
        if (cookies != null) {
            token = Arrays.stream(cookies)
                    .filter(c -> USER_TOKEN_NAME.equals(c.getName()))
                    .findAny()
                    .map(Cookie::getName)
                    .orElse(null);
        }

        if (token == null || !securityService.isAuthenticated(token)) {
            // return to login page
//            request.getRequestDispatcher("/login").forward(request, response);
            response.sendRedirect("/login");
        } else if (securityService.isAuthorized(token, request.getServletPath())) {
            // forward if authorized
            request.getRequestDispatcher(request.getServletPath()).forward(request, response);
        } else {
            //todo: redirect to "Unauthorized"
            response.sendRedirect("/login");
        }
    }

    public void setSecurityService(DefaultSecurityService securityService) {
        this.securityService = securityService;
    }

}
