package com.study.web.shop;

import com.study.web.shop.dao.product.ProductDao;
import com.study.web.shop.dao.product.jdbc.JdbcProductDao;
import com.study.web.shop.dao.security.SecurityDao;
import com.study.web.shop.dao.security.jdbc.JdbcSecurityDao;
import com.study.web.shop.service.product.DefaultProductService;
import com.study.web.shop.service.security.DefaultSecurityService;
import com.study.web.shop.service.security.SecurityService;
import com.study.web.shop.web.filter.SecurityFilter;
import com.study.web.shop.web.servlet.security.SecurityServlet;
import com.study.web.shop.web.servlet.product.AddProductServlet;
import com.study.web.shop.web.servlet.product.AllProductsServlet;
import com.study.web.shop.web.servlet.security.LoginServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import java.util.EnumSet;

public class Application {

    public static void main(String[] args) throws Exception {
//        List<String> acceptedTokens = new ArrayList<>();

        ProductDao productDao = new JdbcProductDao();
        DefaultProductService productService = new DefaultProductService();
        productService.setProductDao(productDao);


        SecurityDao securityDao = new JdbcSecurityDao();
        DefaultSecurityService securityService = new DefaultSecurityService();
        securityService.setSecurityDao(securityDao);

        
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        SecurityFilter securityFilter = new SecurityFilter();
        securityFilter.setSecurityService(securityService);
        context.addFilter(new FilterHolder(securityFilter), "/*", EnumSet.of(DispatcherType.REQUEST));

        //security servlet
//        SecurityServlet securityServlet = new SecurityServlet();
//        securityServlet.setSecurityService(new DefaultSecurityService());
//        context.addServlet(new ServletHolder(securityServlet), "/*");
//        context.addServlet(new ServletHolder(securityServlet), "/security");


        AllProductsServlet allProductsServlet = new AllProductsServlet();
        allProductsServlet.setProductService(productService);
//        allProductsServlet.setAcceptedTokens(acceptedTokens);
        ServletHolder mainPageServletHolder = new ServletHolder(allProductsServlet);

        context.addServlet(mainPageServletHolder, "/products");
        context.addServlet(mainPageServletHolder, "/");

        AddProductServlet addProductServlet = new AddProductServlet();
        addProductServlet.setProductService(productService);
        context.addServlet(new ServletHolder(addProductServlet), "/product/add");

        LoginServlet loginServlet = new LoginServlet();
//        loginServlet.setAcceptedTokens(acceptedTokens);
        loginServlet.setSecurityService(securityService);
        context.addServlet(new ServletHolder(loginServlet), "/login");

        Server server = new Server(8081);
        server.setHandler(context);

        server.start();


    }
}
