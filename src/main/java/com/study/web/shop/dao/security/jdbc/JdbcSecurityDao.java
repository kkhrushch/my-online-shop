package com.study.web.shop.dao.security.jdbc;

import com.study.web.shop.dao.security.SecurityDao;
import com.study.web.shop.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JdbcSecurityDao implements SecurityDao {
    private Connection connection;

    public JdbcSecurityDao() {
        try {
            this.connection = getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to connect");
        }
    }

    @Override
    public List<User> getUsers() {
        List<User> users = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT user_name, user_password, user_role FROM user_detail");
            while (resultSet.next()){
                String userName = resultSet.getString(1);
                String userPassword = resultSet.getString(2);
                String userRole = resultSet.getString(3);
                users.add(new User(userName, userPassword, userRole));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to execute statement");
        }

        return users;
    }

    @Override
    public Map<String, List<String>> getRoleUrl() {
        Map<String, List<String>> result = new HashMap<>();

        try {
            Statement userRolesStatement = connection.createStatement();
            ResultSet userRoles = userRolesStatement.executeQuery("SELECT distinct user_role FROM role_url_privilege");
            while (userRoles.next()){
                String role = userRoles.getString(1);
                PreparedStatement roleUrlsStatement = connection.prepareStatement("SELECT url FROM role_url_privilege WHERE user_role = ?");
                roleUrlsStatement.setString(1, role);
                ResultSet roleUrlsResultSet = roleUrlsStatement.executeQuery();

                List<String> urls = new ArrayList<>();
                while(roleUrlsResultSet.next()){
                    urls.add(roleUrlsResultSet.getString(1));
                }

                result.put(role, urls);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");

        return DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/shop", "postgres", "1234");

    }

}
