package com.study.web.shop.dao.security;

import com.study.web.shop.entity.User;

import java.util.List;
import java.util.Map;

public interface SecurityDao {

    List<User> getUsers();

    Map<String, List<String>> getRoleUrl();
}
