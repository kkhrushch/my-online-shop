package com.study.web.shop.service.security;

import com.study.web.shop.dao.security.SecurityDao;

public interface SecurityService {
    boolean isAuthorized(String token, String url);

    String authenticateAndGetToken(String name, String password);

}
