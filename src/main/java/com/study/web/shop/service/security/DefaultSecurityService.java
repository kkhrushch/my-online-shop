package com.study.web.shop.service.security;

import com.study.web.shop.dao.security.SecurityDao;
import com.study.web.shop.entity.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DefaultSecurityService implements SecurityService {
    private SecurityDao securityDao;

    private List<User> users;
    private Map<String, User> tokenToUserMap = new HashMap<>();
    private Map<String, List<String>> roleUrl;

    public boolean isAuthenticated(String token){
        return tokenToUserMap.containsKey(token);
    }

    public boolean isAuthorized(String token, String url){
        if(tokenToUserMap.containsKey(token)){
            User user = tokenToUserMap.get(token);
            return roleUrl.get(user.getRole()).contains(url);
        } else {
            return false;
        }

    }

    public String authenticateAndGetToken(String name, String password) {
        User user = users.stream()
                .filter(u -> u.getName().equals(name) && u.getPassword().equals(password))
                .findAny()
                .orElse(null);

        String token = null;
        if(user != null){
            token = UUID.randomUUID().toString();
            tokenToUserMap.put(token, user);
        }

        return token;
    }

    public void setSecurityDao(SecurityDao securityDao) {
        this.securityDao = securityDao;

        //todo: move from this method
        users = securityDao.getUsers();
        roleUrl = securityDao.getRoleUrl();
    }

}
